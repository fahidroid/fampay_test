# FamPay React Native Task

andorid [demo](https://youtu.be/EYnHeHleFX8)
ios [demo](https://youtu.be/YqR-uo-B3IE)
### Prerequisites


* React Native
* Android Studio / SDK Installed
* Xcode / iOS Simulator
* Node

Follow the instructions from react-native [docs](https://facebook.github.io/react-native/docs/getting-started)


### Installing

To install the node_modules

```
npm install
```

To run app on android
```
react-native run-android
```

To run app on ios
```
react-native run-ios
```

