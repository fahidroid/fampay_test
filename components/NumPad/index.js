/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import NavigationService from '../../navigation/NavigationService';
export default class NumPad extends Component {
  render() {
    return (
      <>
        <View style={styles.wrapper}>
          {[1, 2, 3, 4, 5, 6, 7, 8, 9].map(num => (
            <TouchableOpacity
              style={{width: '30%', padding: 8, margin: 2}}
              onPress={() => this.props.handleTouchPad(num)}>
              <Text style={styles.numChar}>{num}</Text>
            </TouchableOpacity>
          ))}
        </View>
        <View style={styles.bottomContainer}>
          {this.props.enableFingerPrint ? (
            <TouchableOpacity
              style={{
                flex: 1,
                padding: 8,
                marginLeft: 10,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => NavigationService.navigate('Fingerprint')}>
              <Icon
                name="fingerprint"
                style={[styles.numChar, {fontSize: 25, marginRight: 10}]}
              />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity style={{flex: 1, padding: 8, margin: 2}}>
              <Text style={styles.numChar} />
            </TouchableOpacity>
          )}

          <TouchableOpacity
            style={{flex: 1, padding: 8, margin: 2}}
            onPress={() => this.props.handleTouchPad(0)}>
            <Text style={styles.numChar}>0</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{flex: 1, padding: 8, margin: 2}}
            onPress={() => this.props.handleTouchPad(null)}>
            <Icon
              name="chevron-left"
              style={[styles.numChar, {fontSize: 30, marginRight: 10}]}
            />
          </TouchableOpacity>
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  numChar: {
    fontFamily: 'Gilroy-Bold',
    fontSize: 24,
    textAlign: 'center',
  },
  wrapper: {
    width: '95%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  bottomContainer: {
    width: '95%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
});
