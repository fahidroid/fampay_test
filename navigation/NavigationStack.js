import React from 'react';
import {View, Text, Dimensions, Image} from 'react-native';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Landing from '../screens/Landing';
import SetPin from '../screens/SetPin';
import VerifyPin from '../screens/VerifyPin';
import Auth from '../screens/Auth';
import Home from '../screens/Home';
import MovieOverview from '../screens/MovieOverview';

import Fingerprint from '../screens/Fingerprint';

const AuthStack = createStackNavigator({
  SetPin: {
    screen: SetPin,
    navigationOptions: {header: null},
  },
  VerifyPin: {
    screen: VerifyPin,
    navigationOptions: {header: null},
  },
  Auth: {
    screen: Auth,
    navigationOptions: {header: null},
  },
  Home: {
    screen: Home,
    navigationOptions: {header: null},
  },
  Fingerprint: {
    screen: Fingerprint,
    navigationOptions: {header: null},
  },
  MovieOverview: {
    screen: MovieOverview,
    navigationOptions: {header: null},
  },
});
export default createAppContainer(
  createSwitchNavigator(
    {
      Landing: Landing,
      AuthStack: AuthStack,
    },
    {
      initialRouteName: 'Landing',
    },
  ),
);
