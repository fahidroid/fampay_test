/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  View,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import NavigationService from '../../navigation/NavigationService';
import NumPad from '../../components/NumPad';
export default class SetPin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pinCode: '',
    };
  }

  componentWillUnmount() {
    this.setState({pinCode: ''});
  }

  handleTouchPad = value => {
    if ( Number.isInteger(value)) {
      this.setState(state => {
        return {
          pinCode:
            state.pinCode.length < 4
              ? state.pinCode.concat(value)
              : state.pinCode,
        };
      });
    } else {
      this.setState(state => {
        return {
          pinCode: state.pinCode.substring(0, state.pinCode.length - 1),
        };
      });
    }
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View>
          <Text style={styles.appName}>FamPay </Text>
          <Text style={styles.setPin}>SET PIN</Text>
          <Text style={styles.secondaryText}>
            Your safety is our first priority
          </Text>
          <View style={{flexDirection: 'row', marginTop: 12}}>
            <TextInput
              style={styles.pinCodeChar}
              editable={false}
              value={(this.state.pinCode && this.state.pinCode[0]) || ''}
            />

            <TextInput
              style={styles.pinCodeChar}
              editable={false}
              value={(this.state.pinCode && this.state.pinCode[1]) || ''}
            />

            <TextInput
              style={styles.pinCodeChar}
              editable={false}
              value={(this.state.pinCode && this.state.pinCode[2]) || ''}
            />

            <TextInput
              style={styles.pinCodeChar}
              editable={false}
              value={(this.state.pinCode && this.state.pinCode[3]) || ''}
            />
            <TextInput
              style={[styles.pinCodeChar, {borderBottomColor: '#fff'}]}
              editable={false}
            />
          </View>
          <Text style={styles.infoText}>
            This PIN will be used to authenticate and login to the app
          </Text>
        </View>
        <View>
          <NumPad handleTouchPad={this.handleTouchPad} />
          <TouchableOpacity
            onPress={() =>
              NavigationService.navigate('VerifyPin', {
                pinCode: this.state.pinCode,
              })
            }
            style={[
              styles.proceedButton,
              {
                backgroundColor:
                  this.state.pinCode.length === 4 ? '#000' : '#ccc',
              },
            ]}>
            <Text style={styles.proceedText}>Proceed</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    justifyContent: 'space-between',
    marginLeft: 22,
    marginRight: 22,
    marginBottom: 12,
  },
  appName: {
    fontFamily: 'Gilroy-Bold',
    fontSize: 22,
    marginTop: 16,
  },
  proceedText: {
    color: '#fff',
    fontSize: 16,
    textAlign: 'center',
    fontFamily: 'Gilroy-Bold',
  },
  setPin: {
    fontFamily: 'Gilroy-Regular',
    fontSize: 16,
    marginTop: 16,
  },
  secondaryText: {
    fontFamily: 'Gilroy-SemiBold',
    fontSize: 16,
    marginTop: 8,
  },
  proceedButton: {
    width: '85%',

    padding: 12,
    alignSelf: 'center',
    borderRadius: 8,
  },
  pinCodeChar: {
    color: '#000',
    fontFamily: 'Gilroy-Bold',
    fontSize: 28,
    textAlign: 'center',
    borderBottomColor: 'gray',
    borderBottomWidth: 3,
    width: 30,
    margin: 8,
  },
  infoText: {
    color: 'gray',
    fontFamily: 'Gilroy-Regular',
    fontSize: 14,
    marginTop: 18,
  },
  numChar: {
    fontFamily: 'Gilroy-Bold',
    fontSize: 24,
    textAlign: 'center',
  },
});
