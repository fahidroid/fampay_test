/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  View,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import NavigationService from '../../navigation/NavigationService';
import NumPad from '../../components/NumPad';
import Storage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/Entypo';
import FingerprintScanner from 'react-native-fingerprint-scanner';
export default class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pinCode: '',
      errorText: '',
      fingerprint: 0,
    };
  }

  getData = async key => {
    try {
      const value = await Storage.getItem(key);
      if (value !== null) {
        return value;
      } else {
        return null;
      }
    } catch (e) {
      alert('Something went wrong with asyncstorage');
    }
  };

  componentDidMount() {
    // Get initial fingerprint enrolled
    this.detectFingerprintAvailable();
  }
  componentWillUnmount() {
    this.setState({pinCode: ''});
  }
  detectFingerprintAvailable = () => {
    FingerprintScanner.isSensorAvailable(() => {
      this.setState({fingerprintSensor: true});
    }).catch(error =>
      this.setState({
        errorMessage: error.message,
        biometric: error.biometric,
        fingerprintSensor: false,
      }),
    );
  };
  handleTouchPad = value => {
    if (Number.isInteger(value)) {
      this.setState(state => {
        return {
          pinCode:
            state.pinCode.length < 4
              ? state.pinCode.concat(value)
              : state.pinCode,
        };
      });
    } else {
      this.setState(state => {
        return {
          pinCode: state.pinCode.substring(0, state.pinCode.length - 1),
        };
      });
    }
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View>
          <Text style={styles.appName}>FamPay </Text>

          <Text style={styles.welcomeText}>welcomes you!</Text>
          <Text style={styles.secondaryText}>
            Please enter the PIN to continue
          </Text>
          <View style={{flexDirection: 'row', marginTop: 4}}>
            <TextInput
              style={styles.pinCodeChar}
              editable={false}
              value={(this.state.pinCode && this.state.pinCode[0]) || ''}
            />

            <TextInput
              style={styles.pinCodeChar}
              editable={false}
              value={(this.state.pinCode && this.state.pinCode[1]) || ''}
            />

            <TextInput
              style={styles.pinCodeChar}
              editable={false}
              value={(this.state.pinCode && this.state.pinCode[2]) || ''}
            />

            <TextInput
              style={styles.pinCodeChar}
              editable={false}
              value={(this.state.pinCode && this.state.pinCode[3]) || ''}
            />
            <TextInput
              style={[styles.pinCodeChar, {borderBottomColor: '#fff'}]}
              editable={false}
            />
          </View>
          <Text style={styles.errorText}>{this.state.errorText}</Text>
          <Text style={styles.infoText}>
            This PIN will be used to authenticate and login to the app
          </Text>
        </View>
        <View>
          <NumPad
            handleTouchPad={this.handleTouchPad}
            enableFingerPrint={true}
          />
          <TouchableOpacity
            onPress={async () => {
              let pinCode = await this.getData('pinCode');
              if (pinCode === this.state.pinCode) {
                NavigationService.navigate('Home');
              } else {
                this.setState({errorText: 'Oops. Incorrect Pin'});
              }
            }}
            style={[
              styles.proceedButton,
              {
                backgroundColor:
                  this.state.pinCode.length === 4 ? '#000' : '#ccc',
              },
            ]}>
            <Text style={styles.proceedText}>Unlock</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    justifyContent: 'space-between',
    marginLeft: 22,
    marginRight: 22,
    marginBottom: 12,
  },
  appName: {
    fontFamily: 'Gilroy-Bold',
    fontSize: 22,
    marginTop: 16,
  },
  proceedText: {
    color: '#fff',
    fontSize: 16,
    textAlign: 'center',
    fontFamily: 'Gilroy-Bold',
  },
  setPin: {
    fontFamily: 'Gilroy-Regular',
    fontSize: 16,
    marginTop: 16,
  },
  welcomeText: {
    fontFamily: 'Gilroy-Regular',
    fontSize: 16,
  },
  secondaryText: {
    fontFamily: 'Gilroy-SemiBold',
    fontSize: 16,
    marginTop: 8,
  },
  proceedButton: {
    width: '85%',

    padding: 12,
    alignSelf: 'center',
    borderRadius: 8,
  },
  pinCodeChar: {
    color: '#000',
    fontFamily: 'Gilroy-Bold',
    fontSize: 28,
    textAlign: 'center',
    borderBottomColor: 'gray',
    borderBottomWidth: 3,
    width: 30,
    margin: 8,
  },
  infoText: {
    color: 'gray',
    fontFamily: 'Gilroy-Regular',
    fontSize: 14,
    marginTop: 18,
  },
  errorText: {
    color: 'rgba(255,0,0,0.5)',
    fontFamily: 'Gilroy-Regular',
    fontSize: 14,
    margin: 8,
    marginTop: 0,
  },
  numChar: {
    fontFamily: 'Gilroy-Bold',
    fontSize: 24,
    textAlign: 'center',
  },
});
