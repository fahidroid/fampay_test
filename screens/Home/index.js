/* eslint-disable react-native/no-inline-styles */
import React, {PureComponent} from 'react';
import {
  Text,
  View,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Alert,
  TouchableWithoutFeedback,
  Image,
} from 'react-native';
import avatar from '../../assets/images/avatar-icon-images-4.jpg';
import Storage from '@react-native-community/async-storage';
import Shimmer from '../../components/Shimmer';
import {ScrollView, FlatList} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Rating} from '../../components/star/src/';
import {NavigationActions, StackActions} from 'react-navigation';

import NavigationService from '../../navigation/NavigationService';
export default class Home extends PureComponent {
  state = {
    fingerprint: 0,
    movies: [],
    loading: true,
    enableScrollViewScroll: true,
  };
  getData = async key => {
    try {
      const value = await Storage.getItem(key);
      if (value !== null) {
        return value;
      } else {
        return null;
      }
    } catch (e) {}
  };

  async componentDidMount() {
    this.getData('fingerprint').then(x => {
      this.setState({fingerprint: Number(x) || 0});
    });
    this.page = 1;
    this.fetchMovie(this.page);
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View
          onStartShouldSetResponderCapture={() => {
            this.setState({enableScrollViewScroll: true});
          }}>
          <ScrollView
            contentContainerStyle={{paddingBottom: 90}}
            scrollEnabled={this.state.enableScrollViewScroll}
            ref={myScroll => (this._myScroll = myScroll)}>
            <View style={styles.profileWrapper}>
              <View style={{flexDirection: 'row'}}>
                <Image source={avatar} style={styles.profileImage} />
                <View style={{justifyContent: 'center'}}>
                  <Text style={styles.profileText}>hello swapnil</Text>
                  <Text style={styles.todayText}>today is movie day🤩</Text>
                </View>
              </View>
            </View>

            <View style={styles.blueWrapper}>
              {this.renderFingerPrintItems()}
            </View>
            <Text style={styles.trendingText}>Trending Top 10</Text>
            <View>{this.renderFlatList()}</View>
          </ScrollView>
        </View>
        <View style={styles.bottomContainer}>
          <TouchableOpacity
            onPress={() => {
              const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({routeName: 'SetPin'})],
              });
              this.props.navigation.dispatch(resetAction);
            }}
            style={styles.resetButton}>
            <Text style={styles.resetButtonText}> reset pin</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({routeName: 'Auth'})],
              });
              this.props.navigation.dispatch(resetAction);
            }}
            style={{
              margin: 8,
              backgroundColor: '#000',
              alignSelf: 'center',
              width: '40%',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 8,
              paddingTop: 6,
              paddingBottom: 6,
            }}>
            <Text
              style={{
                fontFamily: 'Gilroy-SemiBold',
                fontSize: 16,
                margin: 8,
                marginBottom: 4,
                color: '#fff',
              }}>
              {' '}
              lock
            </Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
  /////========================================== fetchMovies ===============================////
  fetchMovie = pageNumber => {
    fetch(
      `https://api.themoviedb.org/3/trending/all/day?api_key=a70dbfe19b800809dfdd3e89e8532c9e&page=${pageNumber}`,
    )
      .then(x => x.json())
      .then(res => {
        setTimeout(() => {
          this.setState(state => {
            return {
              loading: false,
              movies: state.movies.concat(res.results),
            };
          });
        });
      })
      .catch(e => {
        console.log(e);
        Alert('Error', 'api failed');
      });
  };
  /////========================================== loadNext ===============================////
  loadNext = () => {
    this.page = this.page + 1;
    this.fetchMovie(this.page);
  };
  /////========================================== renderBlueWrappedItems ===============================////
  renderFingerPrintItems = () => {
    if (Number(this.state.fingerprint) == 0) {
      return (
        <>
          <Text style={styles.fingerprintInfoText}>
            {' '}
            would you like to enable fingerprint auth?
          </Text>
          <TouchableOpacity
            onPress={() =>
              Storage.setItem('fingerprint', '1').then(() =>
                this.setState({fingerprint: 1}),
              )
            }
            style={styles.fingerprintButton}>
            <Text style={styles.fingerprintButtonText}> enable 🔒</Text>
          </TouchableOpacity>
        </>
      );
    } else {
      return (
        <>
          <Text style={styles.fingerprintInfoText}>
            {' '}
            would you like to disable fingerprint auth?
          </Text>
          <TouchableOpacity
            onPress={() =>
              Storage.setItem('fingerprint', '0').then(() =>
                this.setState({fingerprint: 0}),
              )
            }
            style={styles.fingerprintButton}>
            <Text style={styles.fingerprintButtonText}> disable 🔓</Text>
          </TouchableOpacity>
        </>
      );
    }
  };
  /////========================================== renderFlatList ===============================////

  renderFlatList = () => {
    if (this.state.loading && this.page == 1) {
      return (
        <>
          <Placeholder />
          <Placeholder />
          <Placeholder />
          <Placeholder />
        </>
      );
    } else {
      return (
        <View
          onStartShouldSetResponderCapture={() => {
            this.setState({enableScrollViewScroll: true});
            if (
              this._myScroll.contentOffset === 0 &&
              this.state.enableScrollViewScroll === false
            ) {
              this.setState({enableScrollViewScroll: true});
            }
          }}>
          <FlatList
            nestedScrollEnabled
            showsHorizontalScrollIndicator={false}
            data={this.state.movies}
            renderItem={MovieItem}
            keyExtractor={(item, index) => index}
            ListFooterComponent={() => {
              return (
                <View>
                  <TouchableOpacity
                    disabled={this.state.loading}
                    onPress={() => {
                      this.setState({loading: true});
                      this.loadNext();
                    }}
                    style={styles.loadingButton}>
                    <Text style={styles.loadingButtonText}>
                      {' '}
                      {this.state.loading ? 'loading..' : 'load more'}
                    </Text>
                  </TouchableOpacity>
                </View>
              );
            }}
          />
        </View>
      );
    }
  };
}
/////========================================== Shimmer ===============================////

const Placeholder = () => {
  return (
    <View
      style={{
        width: '90%',
        alignSelf: 'center',
        margin: 8,
        flexDirection: 'row',
      }}>
      <Shimmer
        autoRun={true}
        visible={false}
        style={{width: 100, height: 100, margin: 4}}
      />
      <View>
        <Shimmer
          autoRun={true}
          visible={false}
          style={{width: '80%', height: '5%', margin: 4}}
        />
        <Shimmer
          autoRun={true}
          visible={false}
          style={{width: '70%', height: '5%', margin: 4}}
        />
        <Shimmer
          autoRun={true}
          visible={false}
          style={{width: '50%', height: '5%', margin: 4}}
        />
      </View>
    </View>
  );
};
/////========================================== Movie Item ===============================////

const MovieItem = ({item, index}) => {
  return (
    <TouchableWithoutFeedback
      onPress={() =>
        NavigationService.navigate('MovieOverview', {movie: item})
      }>
      <View
        style={{
          backgroundColor: '#fff',
          height: 100,
          width: '90%',
          alignSelf: 'center',
          margin: 12,
          borderRadius: 15,
          flexDirection: 'row',
        }}>
        <View>
          <Image
            style={{
              position: 'absolute',
              top: -10,
              left: 10,
              width: 60,
              height: 105,
              borderRadius: 12,
            }}
            source={{
              uri: `https://image.tmdb.org/t/p/w200${item.poster_path}`,
            }}
          />
        </View>
        <View style={{margin: 8, marginLeft: 70, width: '70%'}}>
          <Text
            style={{
              fontFamily: 'Gilroy-Bold',
              color: '#000',
              fontSize: 14,
              margin: 4,
              marginBottom: 2,
            }}>
            {item.title || item.name}
          </Text>
          <Text
            style={{
              fontFamily: 'Gilroy-Light',
              color: 'gray',
              fontSize: 14,
              margin: 4,
            }}>
            {item.original_language.toUpperCase()} | {item.adult ? 'R' : 'UA'} |{' '}
            {item.first_air_date || item.release_date}
          </Text>
          <View style={{flexDirection: 'row', margin: 4}}>
            <Rating
              imageSize={20}
              count={5}
              startingValue={item.vote_average / 2}
              readonly={true}
            />
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  bottomContainer: {
    elevation: 3,
    borderTopColor: '#eee',
    borderTopWidth: 1,
    position: 'absolute',
    bottom: 0,
    padding: 12,
    backgroundColor: '#fff',
    width: '100%',
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: '#ddd',
  },
  fingerprintInfoText: {
    fontFamily: 'Gilroy-SemiBold',
    fontSize: 16,
    margin: 8,
    marginBottom: 4,
    color: '#fff',
  },
  fingerprintButton: {
    margin: 8,
    backgroundColor: '#000',
    alignSelf: 'flex-end',
    width: 100,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    paddingTop: 2,
    paddingBottom: 2,
  },
  fingerprintButtonText: {
    fontFamily: 'Gilroy-SemiBold',
    fontSize: 16,
    margin: 8,
    marginBottom: 4,
    color: '#fff',
  },
  loadingButton: {
    margin: 8,
    backgroundColor: '#000',
    alignSelf: 'center',
    width: 100,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    paddingTop: 2,
    paddingBottom: 2,
  },
  loadingButtonText: {
    fontFamily: 'Gilroy-SemiBold',
    fontSize: 16,
    margin: 8,
    marginBottom: 4,
    color: '#fff',
  },
  profileWrapper: {
    margin: 16,
    width: '90%',
    backgroundColor: '#fff',
    borderRadius: 15,
    padding: 16,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  profileImage: {height: 70, width: 70, margin: 8},
  profileText: {
    fontFamily: 'Gilroy-SemiBold',
    fontSize: 20,
    margin: 8,
    marginBottom: 4,
  },
  todayText: {
    fontFamily: 'Gilroy-Light',
    fontSize: 16,
    margin: 0,
    marginLeft: 8,
  },
  blueWrapper: {
    margin: 4,
    width: '90%',
    backgroundColor: '#3090C7',
    borderRadius: 15,
    padding: 8,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  trendingText: {
    marginLeft: 22,
    marginTop: 22,
    fontFamily: 'Gilroy-Bold',
    fontSize: 22,
  },
  resetButton: {
    margin: 8,
    backgroundColor: '#fff',
    borderColor: '#000',
    borderWidth: 1,
    alignSelf: 'center',
    width: '40%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    paddingTop: 6,
    paddingBottom: 6,
  },
  resetButtonText: {
    fontFamily: 'Gilroy-SemiBold',
    fontSize: 16,
    margin: 8,
    marginBottom: 4,
    color: '#000',
  },
});
