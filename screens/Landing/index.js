import React, {Component} from 'react';
import {SafeAreaView, Text, StyleSheet} from 'react-native';
import Storage from '@react-native-community/async-storage';
import NS from '../../navigation/NavigationService';
class Landing extends Component {
  getData = async key => {
    try {
      const value = await Storage.getItem(key);
      if (value !== null) {
        return value;
      } else {
        return null;
      }
    } catch (e) {
      alert('Something went wrong with asyncstorage');
    }
  };

  async componentDidMount() {
    let pinCode = await this.getData('pinCode');
    setTimeout(() => {
      if (pinCode == null) {
        NS.navigate('SetPin');
      } else {
        NS.navigate('Auth');
      }
    }, 3000);
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Text style={styles.appName}>FamPay</Text>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  appName: {
    fontFamily: 'Gilroy-Bold',
    color: '#000',
    fontSize: 48,
    textAlign: 'center',
  },
});

export default Landing;
