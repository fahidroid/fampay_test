/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  View,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Alert,
  ImageBackground,
  Image,
  Platform,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import NavigationService from '../../navigation/NavigationService';
import NumPad from '../../components/NumPad';
import Storage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/Entypo';
import {Rating} from '../../components/star/src';
export default class MovieOverview extends Component {
  render() {
    let movie = this.props.navigation.getParam('movie');
    return (
      <SafeAreaView style={styles.container}>
        <TouchableOpacity
          onPress={() => this.props.navigation.goBack()}
          style={{
            margin: 2,
            alignSelf: 'flex-start',
          }}>
          <Icon
            name="chevron-left"
            style={[styles.icon, {fontSize: 40, marginRight: 10, margin: 2}]}
          />
        </TouchableOpacity>
        <View style={styles.wrapper}>
          <Image
            style={styles.movieImage}
            source={{
              uri: `https://image.tmdb.org/t/p/w500${movie.poster_path}`,
            }}
          />
          <View style={{padding: 8}}>
            <Text style={styles.movieTitle}>{movie.title || movie.name}</Text>
            <Text style={styles.infos}>
              {movie.original_language && movie.original_language.toUpperCase()}{' '}
              | {movie.adult ? 'R' : 'UA'} |{' '}
              {movie.first_air_date || movie.release_date}
            </Text>
            <View style={{flexDirection: 'row', margin: 4}}>
              <Rating
                imageSize={20}
                count={5}
                startingValue={movie.vote_average / 2}
                readonly={true}
              />
            </View>
            <Text numberOfLines={4} style={styles.overview}>
              {movie.overview}
            </Text>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack()}
              style={styles.trailerButton}>
              <Text style={styles.trailerText}> watch trailer</Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#eee',
    flex: 1,
  },
  wrapper: {
    width: '75%',
    alignSelf: 'center',
    height: '95%',
    backgroundColor: '#fff',
    padding: 0,
    marginBottom: 20,
    borderRadius: 8,
  },
  movieImage: {
    height: '47%',
    width: '100%',
    margin: 8,
    marginLeft: 0,
    marginRight: 0,
    padding: 0,
    borderRadius: 8,
    alignSelf: 'center',
  },
  movieTitle: {fontFamily: 'Gilroy-Bold', fontSize: 18, margin: 4},
  infos: {
    fontFamily: 'Gilroy-Light',
    fontSize: 14,
    color: 'gray',
    margin: 4,
  },
  trailerButton: {
    margin: 8,
    backgroundColor: '#000',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    paddingTop: 6,
    paddingBottom: 6,
  },
  trailerText: {
    fontFamily: 'Gilroy-SemiBold',
    fontSize: 16,
    margin: 8,
    marginBottom: 4,
    color: '#fff',
  },
  overview: {
    fontFamily: 'Gilroy-Light',
    fontSize: 14,
    color: 'gray',
    margin: 4,
  },

  icon: {
    fontFamily: 'Gilroy-Bold',
    fontSize: 24,
  },
});
