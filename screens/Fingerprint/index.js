/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  View,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Alert,
  Platform,
  ToastAndroid,
} from 'react-native';
import NavigationService from '../../navigation/NavigationService';
import Icon from 'react-native-vector-icons/Entypo';
import FingerprintScanner from 'react-native-fingerprint-scanner';

export default class Fingerprint extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pinCode: '',
      errorText: '',
      errorMessage: '',
      biometric: '',
    };
  }
  componentDidMount() {
    this.scanFinger();
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View>
          <View style={{flexDirection: 'row', marginTop: 16, marginLeft: -6}}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Icon
                name="chevron-left"
                style={[styles.numChar, {fontSize: 30, marginRight: 10}]}
              />
            </TouchableOpacity>
            <Text style={styles.appName}>FamPay </Text>
          </View>

          <Text style={styles.secondaryText}>Touch the fingerprint sensor</Text>
          <Text style={styles.errorText}>
            {this.state && this.state.errorMessage}{' '}
            {this.state && this.state.biometric}
          </Text>
        </View>
      </SafeAreaView>
    );
  }

  scanFinger = () => {
    if (Platform.OS === 'android') {
      FingerprintScanner.authenticate({
        onAttempt: this.handleAuthenticationAttempted,
      })
        .then(() => {
          Platform.OS === 'android' &&
            ToastAndroid.show('Success !', ToastAndroid.LONG);
          NavigationService.navigate('Home');
        })
        .catch(error => {
          console.log(error);
          this.setState({
            errorMessage: error.message,
            biometric: error.biometric,
          });
        });
    } else {
      FingerprintScanner.authenticate({
        description: 'Scan your fingerprint on the device scanner to continue',
      })
        .then(() => {
          Alert.alert(
            'Fingerprint Authentication',
            'Authenticated successfully',
          );
        })
        .catch(error => {
          console.log(error);
          this.setState({
            errorMessage: error.message,
            biometric: error.biometric,
          });
        });
    }
  };

  handleAuthenticationAttempted = error => {
    this.setState({errorMessage: error.message});
  };

  handleTouchPad = value => {
    if ( Number.isInteger(value)) {
      this.setState(state => {
        return {
          pinCode:
            state.pinCode.length < 4
              ? state.pinCode.concat(value)
              : state.pinCode,
        };
      });
    } else {
      this.setState(state => {
        return {
          pinCode: state.pinCode.substring(0, state.pinCode.length - 1),
        };
      });
    }
  };
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    justifyContent: 'space-between',
    marginLeft: 22,
    marginRight: 22,
    marginBottom: 12,
  },
  appName: {
    fontFamily: 'Gilroy-Bold',
    fontSize: 22,
  },
  proceedText: {
    color: '#fff',
    fontSize: 16,
    textAlign: 'center',
    fontFamily: 'Gilroy-Bold',
  },
  setPin: {
    fontFamily: 'Gilroy-Regular',
    fontSize: 16,
    marginTop: 16,
  },
  secondaryText: {
    fontFamily: 'Gilroy-SemiBold',
    fontSize: 16,
    marginTop: 8,
  },
  proceedButton: {
    width: '85%',

    padding: 12,
    alignSelf: 'center',
    borderRadius: 8,
  },
  pinCodeChar: {
    color: '#000',
    fontFamily: 'Gilroy-Bold',
    fontSize: 28,
    textAlign: 'center',
    borderBottomColor: 'gray',
    borderBottomWidth: 3,
    width: 30,
    margin: 8,
  },
  infoText: {
    color: 'gray',
    fontFamily: 'Gilroy-Regular',
    fontSize: 14,
    marginTop: 18,
  },
  errorText: {
    color: 'rgba(255,0,0,0.5)',
    fontFamily: 'Gilroy-Regular',
    fontSize: 14,
    margin: 8,
    marginTop: 0,
  },
  numChar: {
    fontFamily: 'Gilroy-Bold',
    fontSize: 24,
    textAlign: 'center',
  },
});
