/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import {View} from 'react-native';
import Navigator from './navigation';

const App = () => {
  return (
    // themes here
    <Navigator />

    //
  );
};

export default App;
